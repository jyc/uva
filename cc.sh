#!/bin/sh

gcc -lm -lcrypt -O2 -pipe -ansi -pedantic -Wall -o $1 $1.c
