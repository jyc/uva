#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#define MIN 1
#define MAX 999999

int *cache;

int cycleLength(int x) {
	int length = 0, n = x;
	while (n != 1) {
		if (n >= MIN && n <= MAX && cache[n-MIN] != 0) {
			cache[x-MIN] = length + cache[n-MIN];
			return length + cache[n-MIN];
		}
		if (n % 2 != 0) {
			n = 3*n + 1;
		} else {
			n = n/2;
		}
		length++;
	}
	cache[x-MIN] = length + 1;
	return length + 1;
}

int main(void) {
	int i, j;
	cache = calloc(MAX-MIN+1, sizeof(int));
	while (true) {
		int ret, max = 0, n;
		bool swapped = false;
		ret = scanf("%d %d", &i, &j);
		if (ret == EOF || ret != 2) {
			return 0;
		}

		if (i > j) {
			int temp;
			temp = i;
			i = j;
			j = temp;

			swapped = true;
		}

		for (n = i; n <= j; n++) {
			int l = cycleLength(n);
			if (l > max) {
				max = l;
			}
		}

		if (swapped) {
			printf("%d %d %d\n", j, i, max);
		} else {
			printf("%d %d %d\n", i, j, max);
		}
	}
	free(cache);

	return 0;
}
